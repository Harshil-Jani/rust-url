extern crate regex;
use regex::Regex;
use std::io;
fn main() {
    println!("Enter your URL : ");
    let mut url = String::new();
    io::stdin().read_line(&mut url).expect("Provide Valid URL !");
    if search_query(&mut url) {
        println!("Linking Direct Search Results or Youtube Links is not Allowed.");
        return;
    }
    if vaild_url(&mut url){
        println!("URL is valid");
    }else{
        println!("URL is Invalid");
    }
    
}

fn search_query (test_url: &String) -> bool{
    let check = Regex::new(r"\?q|\?v").unwrap();
    return check.is_match(test_url);
}

fn vaild_url (check_url: &String) -> bool{
    let checks = Regex::new(r"^(https://|http://|www)").unwrap();
    let chunks: Vec<_> = check_url.split('.').collect();
    if chunks.len() < 3 {
        return false;
    }
    for part in chunks{
        if part=="ww" || part=="w"{
            println!("www is the valid domain.");
            return false;
        }
        if check_paths(part) == false {
            println!("Invalid URL");
            return false;
        }
    }
    return checks.is_match(check_url);
}

fn check_paths (check_path: &str) -> bool {
    let regex_path = Regex::new(r"^[a-zA-Z0-9]").unwrap();
    return regex_path.is_match(check_path);
}
